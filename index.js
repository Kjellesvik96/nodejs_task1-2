const readline = require('readline');
const { userInfo } = require('os');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Choose an option:\n1: Read pacakge.json\n2: Display OS info\n3: Start HTTP server\nType a numer:\n',
            (userinput) => {
                if (userinput == 1) {
                    read();
                    rl.close();
                }
                else if (userinput == 2) {
                    display();
                    rl.close();
                }
                else if (userinput == 3) {
                    start();
                    rl.close();
                } else {
                    console.log("Wrong input");
                    rl.close();
                }
            });

function read() {
    console.log('Reading package.json file');
    const fs = require('fs');

    fs.readFile(__dirname + '/package.json', 'utf-8', (err, content) => {
        console.log(content);
    });
}
function display() {
    console.log('Getting info...');
    const os = require('os');
    console.log(`SYSTEM MEMORY: ${os.totalmem()}\nFREE MEMORY: ${os.freemem()}\nCPU CORES: ${os.cpus()}\nARCH: ${os.arch()}\nPLATFORM: ${os.platform()}\nRELEASE: ${os.release()}\nUSER: ${os.userInfo()}`);
}
function start() {
    console.log('Starting HTTP server...');
    const { PORT = 3000} = process;

    const http = require('http').createServer(async function(req, res) {
    
     try {
            res.write('Hello World!')
            return res.end();
        } catch (e) {
            return res.end(e.message);
        }
    });
    
    http.listen(PORT, ()=> {
        console.log('Server running on port ' + PORT)
    });
}